class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :accounts, :hashword, :password_digest
  end
end
