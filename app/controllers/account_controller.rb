class AccountController < ApplicationController
  def new
    @account = Account.new
  end

  def create
    @account = Account.new(account_params)
    if @account.save!
      session[:user_id] = @account.id
      redirect_to '/articles'
    else
      redirect_to '/account/new'
    end
  end

  private
  def account_params
    params.require(:account).permit(:username, :password);
  end
end
