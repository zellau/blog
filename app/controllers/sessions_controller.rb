class SessionsController < ApplicationController
  def new
  end

  def create
    @account = Account.find_by_username(params[:session][:username])
    if @account && @account.authenticate(params[:session][:password])
      session[:user_id] = @account.id
      redirect_to '/articles'
    else
      redirect_to '/login'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to '/articles'
  end

end
