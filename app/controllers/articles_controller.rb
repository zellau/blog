class ArticlesController < ApplicationController

  http_basic_authenticate_with name: "admin", password: "iamtheblogadmin", except: [:new, :create, :index, :show]

  #index
  def index
    @articles = Article.all
    @account = Account.find(session[:user_id]) if session[:user_id] != nil
    @my_articles = @account.articles.all if @account != nil
  end

  #show
  def show
    @article = Article.find(params[:id])
  end

  #new
  def new
    if current_user
      @article = Article.new
    else
      redirect_to "/articles"
    end
  end

  #edit
  def edit
    @account = Account.find(session[:user_id])
    @article = @account.articles.find(params[:id])
#    @article = Article.find(params[:id])
  end

  #create
  def create
    @account = Account.find(session[:user_id])
    @article = @account.articles.create(article_params)

    @article.text = @text

    redirect_to @article

  end

  #update
  def update
    @account = Account.find(session[:user_id])
    @article = @account.articles.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  #destroy
  def destroy
    @account = Account.find(session[:user_id])
    @article = @account.articles.find(params[:id])
    @article.destroy
    
    redirect_to articles_path
  end

  private
  def article_params
    params.require(:article).permit(:title, :text)
  end
end
